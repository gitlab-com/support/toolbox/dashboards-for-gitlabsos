#!/bin/bash

docker-compose exec druid scripts/process-gitlabsos.sh

docker-compose exec superset superset fab create-admin \
               --username admin \
               --firstname Superset \
               --lastname Admin \
               --email admin@superset.com \
               --password admin
docker-compose exec superset superset db upgrade
docker-compose exec superset superset init

cookie=$(curl -sSL -D - http://localhost:8088/login -o /dev/null | grep Set-Cookie | cut -d : -f 2 | sed 's/ //')
bearer=$(curl --header 'Content-Type: application/json' --header "Cookie: $cookie" -XPOST  http://localhost:8088/api/v1/security/login --data '{ "username": "admin", "password": "admin", "provider": "db"}' | jq -r '.access_token')
csrf_token=$(curl --header "Authorization: Bearer $bearer" --header "Cookie: $cookie" http://localhost:8088/api/v1/security/csrf_token/ | jq -r '.result')

# Initialize database
curl -X 'POST' \
  'http://127.0.0.1:8088/api/v1/database/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H "X-CSRFToken: $csrf_token" \
  -H "Authorization: Bearer $bearer" \
  -H 'Origin: http://127.0.0.1:8088' \
  -H "Cookie: $cookie" \
  -d '{
  "database_name": "Druid",
  "sqlalchemy_uri": "druid://druid:8888/druid/v2/sql"
}'

# Initialize datasets
for dataset in ./datasets/*; do
  curl -X 'POST' \
    'http://127.0.0.1:8088/api/v1/dataset/' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -H "X-CSRFToken: $csrf_token" \
    -H "Authorization: Bearer $bearer" \
    -H 'Origin: http://127.0.0.1:8088' \
    -H "Cookie: $cookie" \
    -d @${dataset}
done

# Import dashboards
for dashboard in ./dashboards/*; do
  curl -X 'POST' \
    'http://127.0.0.1:8088/api/v1/dashboard/import/' \
    -H 'accept: application/json' \
    -H 'Content-Type: multipart/form-data' \
    -H "X-CSRFToken: $csrf_token" \
    -H "Authorization: Bearer $bearer" \
    -H 'Origin: http://127.0.0.1:8088' \
    -H "Cookie: $cookie" \
    -F "formData=@${dashboard}" \
    -F "overwrite=true"
done
